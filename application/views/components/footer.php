<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/footer.css'); ?>" media="screen" />

<footer id="sticky-footer" class="py-4 bg-secondary text-white">
	<div class="container text-center">
		<small>Copyright &copy; Pelis Favoritas</small>
	</div>
</footer>