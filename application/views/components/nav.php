<?php
defined('BASEPATH') or exit('No direct script access allowed');

$home = str_replace("/index.php", "", site_url('home/'));
$dashboard = str_replace("/index.php", "", site_url('home/dashboard'));
$login = str_replace("/index.php", "", site_url('home/login'));
$logout = str_replace("/index.php", "", site_url('home/logout'));

$logeo = $this->session->userdata('username') == "";
$log = $logeo ? 'Sign in' : 'Sign out';
$msg_dashboard = $logeo ? '' : 'Dashboard';
$action = $logeo ? $login : $logout;
$action2 = $logeo ? '' : $dashboard;

?>

<meta charset="utf-8">
<title>Pelis Favoritas</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/6aa0a94303.js" crossorigin="anonymous"></script>

<nav class="navbar sticky-top navbar-dark bg-dark shadow navbar-expand-lg py-3 justify-content-between">
	<div class="container">

		<a href="<?php echo $home; ?>" class="navbar-brand">
			<img src="<?php echo base_url('assets/ticket.png'); ?>" width="45" alt="" class="d-inline-block align-middle mr-2">
			<span class="text-uppercase font-weight-bold">Pelis Favoritas</span>
		</a>

		<button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"></span>
		</button>

		<div id="navbarSupportedContent" class="collapse navbar-collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="<?php echo $home; ?>" class="nav-link">Home
						<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item active">
					<a class="nav-link" href="<?php echo $action2; ?>" class="nav-link"><?php echo $msg_dashboard; ?>
						<span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item"><a href="<?php echo $action; ?>" class="nav-link"><?php echo $log; ?></a></li>
			</ul>
		</div>

	</div>
</nav>