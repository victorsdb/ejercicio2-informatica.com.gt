<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<style>
    .fondo {
        color: white;
        background-image: url("https://source.unsplash.com/random/1920x1080");
        background-position: center;
        background-repeat: no-repeat;
        height: 90vh;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .texto {
        font-size: 75px;
        font-family: 'Poppins', 'Courier New', Courier, monospace;
        margin: 0 10px 0 10px;
        padding: 10px 20px 10px 20px;
        color: white;
        text-shadow: 2px 8px 6px rgba(0, 0, 0, 0.2),
            0px -5px 35px rgba(255, 255, 255, 0.3);
    }
</style>

<div class="fondo vh-center">
    <div class="texto text-uppercase text-weight-bold">Bienvenido a Peli Favoritas.</div>
</div>