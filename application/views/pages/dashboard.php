<?php
defined('BASEPATH') or exit('No direct script access allowed');

$username = $this->session->userdata('username');
$title = "";
$year = "";
$poster = "";



$this->load->model('home_model');

if (isset($_POST['buscar'])) {

    $titulo = $this->input->post('titulo');

    if ($titulo != "") {

        $json = file_get_contents("http://www.omdbapi.com/?t=" . str_replace(" ", "+", $titulo) . "&apikey=c571e873");
        $datos = json_decode($json, true);

        if (isset($datos['Title'])) {
            $this->session->set_userdata(array(
                'year'     =>     $datos['Year']
            ));
            $this->session->set_userdata(array(
                'poster'     =>   $datos['Poster']
            ));
            $this->session->set_userdata(array(
                'title'     =>    $datos['Title']
            ));
        } else {
            $this->session->unset_userdata('title');
            $this->session->unset_userdata('year');
            $this->session->unset_userdata('poster');
        }
    } else {
        $this->session->unset_userdata('title');
        $this->session->unset_userdata('year');
        $this->session->unset_userdata('poster');
    }
} else if (isset($_POST['agregar'])) {
    echo "<META HTTP-EQUIV='Refresh' CONTENT='0; url=''>";
    $title = $this->session->userdata('title');
    $year = $this->session->userdata('year');
    $poster = $this->session->userdata('poster');
    $this->home_model->insert_movie($title, $year, $poster, $username);
    $message = "Se agrego pelicula correctamente a la bd";
    $this->session->set_userdata(array(
        'message'     =>    $message
    ));
} else if (isset($_POST['borrar'])) {
    echo "<META HTTP-EQUIV='Refresh' CONTENT='0; url=''>";
    $title = $this->session->userdata('title');
    $this->home_model->delete_movie($title);
    $message = "Se elimino la pelicula correctamente a la bd";
    $this->session->set_userdata(array(
        'message'     =>    $message
    ));
}


?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dashboard.css'); ?>" media="screen" />

<style>

</style>
<div class="container-fluid">
    <form method="post" action="">
        <div class="row">
            <div class="col-9 justify-content-center" style="padding: 50px 15px 200px 15px;">
                <h1 class="text-uppercase text-center">Películas Favoritas</h1>
                <div class="row justify-content-center">
                    <?php
                    if ($fetch_data->num_rows() > 0) {
                        foreach ($fetch_data->result() as $row) {
                    ?>
                            <div class="card" style="width: 18rem; margin: 15px; border-radius: 60px 15px 60px 15px;">
                                <img class="card-img-top" style="border-radius: 60px 15px 0px 0px;" src="<?php echo $row->imagen; ?>" alt="Card image cap">
                                <div class="card-body justify-content-center">

                                    <div class="container" style="margin-bottom: 25px;">
                                        <div class="row">
                                            <div class="col-4"> <strong>Título: </strong> </div>
                                            <div class="col-8"><?php echo $row->titulo; ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4"><strong>Año:</strong></div>
                                            <div class="col-8"><?php echo $row->anio; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                        }
                    } else {
                        ?>

                    <?php
                    }
                    ?>
                </div>

            </div>

            <div class="col-3">
                <div class="busqueda">
                    <div class="row  justify-content-center" style="padding: 50px;">
                        <div class="input-group" style="margin-top: 1rem; width: 30rem;">
                            <input type="text" class="form-control" name="titulo" placeholder="título de pelicula">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" name="buscar">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center" style="<?php echo isset($datos["Title"]) ? "" : "display: none;" ?>">

                        <div class="card" style="width: 18rem; margin: 0px 15px 50px 15px; border-radius: 15px 15px 15px 15px;">
                            <img class="card-img-top" style="border-radius: 15px 15px 0px 0px;" src="<?php echo isset($datos["Poster"]) ? $datos["Poster"] : ""; ?>" alt="Card image cap">
                            <div class="card-body justify-content-center">

                                <div class="container" style="margin-bottom: 25px;">
                                    <div class="row">
                                        <div class="col"> <strong>Título: </strong> </div>
                                        <div class="col"> <?php echo isset($datos["Title"]) ? $datos["Title"] : ""; ?> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><strong>Año:</strong></div>
                                        <div class="col"> <?php echo isset($datos["Year"]) ? $datos["Year"] : ""; ?>" </div>
                                    </div>
                                </div>


                                <?php
                                $var = $this->home_model->exits_movie($username, isset($datos["Title"]) ? $datos["Title"] : "");
                                if ($var) {
                                    echo '
                                <button type="submit" name="borrar" class="css-button-warn">
                                    <span class="css-button-warn-icon"><i class="fa fa-remove" aria-hidden="true" id="i_pos"></i></span>
                                    <span class="css-button-warn-text"  style="width: 190px">Remover de Favoritos</span>
                                </button>';
                                } else {
                                    echo '                            
                                <button class="css-button" type="submit" name="agregar">
                                    <span class="css-button-icon"><i class="fa fa-star-o" aria-hidden="true" id="i_pos"></i></span>
                                    <span class="css-button-text" style="width: 190px">Agregar a Favoritos</span>
                                </button>';
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <?php
                        if ($this->session->userdata('message') != "") {
                            echo '<div class="alert alert-info" style="margin: 0px 40px 40px 40px; border-radius: 20px;">';
                            echo $this->session->userdata('message');
                            echo "</div>";
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </div>

                </div>
            </div>



        </div>

    </form>

</div>