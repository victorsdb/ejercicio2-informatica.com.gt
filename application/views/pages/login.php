<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/login.css'); ?>" media="screen" />


<div class="container" style="margin-top: 100px;">
    <div class="row justify-content-center">
        <div class="col-sm-6 col-md-4 col-md-offset-4">

            <h1 class="text-center diplay-1">Iniciar Sesión</h1>

            <div class="account-wall">
                <img class="profile-img" src="<?php echo base_url('assets/camera.png') ?>" alt="">
                <form method="post" action="<?php echo base_url(); ?>home/login_validation" class="form-signin">

                    <input type="text" name="username" class="form-control" placeholder="usuario" required autofocus>
                    <input type="password" name="password" class="form-control" placeholder="contraseña" required>

                    <input type="submit" name="insert" value="Iniciar" class="btn btn-lg btn-primary btn-block btn-info" />

                </form>
            </div>
        </div>
    </div>
</div>