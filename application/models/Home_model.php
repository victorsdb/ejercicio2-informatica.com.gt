<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home_model extends CI_Model
{
     function can_login($username, $password)
     {
          $query = $this->db->select('*')
               ->from('usuario')
               ->where('usuario', $username)
               ->where('contrasenia', $password)
               ->get();

          if ($query->num_rows() > 0) {
               return true;
          } else {
               return false;
          }
     }

     function insert_movie($title, $year, $image, $username)
     {
          $query = $this->db->insert('pelicula', [
               'titulo'   => $title,
               'anio'     => $year,
               'imagen'   => $image,
               'usuario'  => $username
          ]);
     }

     function get_movies($username)
     {
          $query = $this->db->select('*')
               ->from('pelicula')
               ->where('usuario', $username)
               ->get();
          return $query;
     }

     function exits_movie($username, $title)
     {
          $query = $this->db->select('*')
               ->from('pelicula')
               ->where('usuario', $username)
               ->where('titulo', $title)
               ->get();

          if ($query->num_rows() > 0) {
               return true;
          } else {
               return false;
          }
     }

     function delete_movie($title)
     {
          $this->db->where('titulo', $title)
                   ->delete('pelicula');
     }
}
