<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('components/nav');
		$this->load->view('pages/home');
		$this->load->view('components/footer');
	}

	public function login()
	{
		$this->load->view('components/nav');
		$this->load->view('pages/login');
		$this->load->view('components/footer');
	}

	function login_validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run()) {

			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$this->load->model('home_model');
			if ($this->home_model->can_login($username, $password)) {
				$session_data = array(
					'username'     =>     $username
				);
				$this->session->set_userdata($session_data);
				redirect(base_url() . 'home/dashboard');
			} else {
				$this->session->set_flashdata('error', 'Invalid Username and Password');
				redirect(base_url() . 'home/login');
			}
		} else {
			$this->login();
		}
	}

	function get_data(){

	}

	function dashboard()
	{
		$this->load->model("home_model");
		

		if ($this->session->userdata('username') != "") {

			$data["fetch_data"] = $this->home_model->get_movies($this->session->userdata('username'));

			$this->load->view('components/nav');
			$this->load->view('pages/dashboard', $data);
			$this->load->view('components/footer');
		} else {
			redirect(base_url() . 'home/login');
		}
	}

	function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('title');
		$this->session->unset_userdata('year');
		$this->session->unset_userdata('poster');
		redirect(base_url() . 'home');
	}

}
