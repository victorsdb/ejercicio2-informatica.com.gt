DROP DATABASE IF EXISTS ejercicio2;
CREATE DATABASE IF NOT EXISTS ejercicio2;

USE ejercicio2;

CREATE TABLE usuario (
	usuario VARCHAR(15) NOT NULL,
	contrasenia VARCHAR(15) NOT NULL, 
	CONSTRAINT usuario_pk PRIMARY KEY (usuario)
);

CREATE TABLE pelicula (
	id INTEGER NOT NULL AUTO_INCREMENT,
	titulo VARCHAR(150) NOT NULL,
	imagen VARCHAR(150) NOT NULL,
	anio INTEGER NOT NULL,
	usuario VARCHAR(15) NOT NULL, 
	CONSTRAINT pelicula_pk 
		PRIMARY KEY (id),
	CONSTRAINT pelicula_fk 
		FOREIGN KEY (usuario) 
			REFERENCES usuario(usuario)
);

INSERT INTO usuario (usuario, contrasenia) VALUES ('admin', 'admin1234$');
INSERT INTO usuario (usuario, contrasenia) VALUES ('admin2', 'admin1234$');
INSERT INTO usuario (usuario, contrasenia) VALUES ('admin3', 'admin1234$');
INSERT INTO usuario (usuario, contrasenia) VALUES ('admin4', 'admin1234$');
INSERT INTO usuario (usuario, contrasenia) VALUES ('admin5', 'admin1234$');

SELECT * FROM usuario;
SELECT * FROM pelicula;

